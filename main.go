package main

import (
    "fmt"
    "net/http"
)

func main() {
    http.HandleFunc("/", HelloServer)
    fmt.Print("Microservice 3 server listening on 0.0.0.0:8080")
    http.ListenAndServe(":8080", nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Microservice 3 says Hello, %s!", r.URL.Path[1:])
}
